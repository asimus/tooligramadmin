<?php

namespace Tooligram\Controllers;

use Tooligram\Models\Logs;

class Tooligram {
    public static function check($data){
        $key = !empty($data['key']) ? $data['key'] : false;

        if(!$key) return [
            'error' => [
                'msg' => '<div class="alert alert-danger">
                            <strong>'.Lang::translate('ERROR').'!</strong>
                            '.Lang::translate('ERROR_EMPTY_KEY').'
                        </div>'
            ]
        ];

        $url = 'http://islandlab.ru/soft/islandgrampr/checkdays2.php?key='.$key;

        $result = self::request($url);

        $logs = Logs::add(3,$key, false, $result);

        if($result == 'NO_KEY') return [
            'error' => [
                'msg' => '<div class="alert alert-danger">
                            <strong>'.Lang::translate('ERROR').'!</strong>
                            '.Lang::translate('ERROR_NOT_FOUND_KEY').'
                        </div>'
            ]
        ];        

        $days = str_replace('days =', '', $result);

        return [
            'success' => [
                'msg' => '<div class="alert alert-success">
                            <strong>'.Lang::translate('SUCCESS').'!</strong>
                            '.Lang::translate('QT_DAYS_KEY').$days.'
                        </div>'
            ]
        ];
    }

    public static function changeKey($data){
        $old = !empty($data['old']) ? $data['old'] : false;
        $new = !empty($data['new']) ? $data['new'] : false;

        $pass = \Yii::$app->params['func'][1]['password'];

        if(!$old || !$new) return [
            'error' => [
                'msg' => '<div class="alert alert-danger">
                            <strong>'.Lang::translate('ERROR').'!</strong>
                            '.Lang::translate('ERROR_EMPTY_KEY_OLD_NEW').'
                        </div>'
            ]
        ];

        $url = 'http://islandlab.ru/soft/islandgrampr/pp.php?old='.$old.'&new='.$new.'&pass='.$pass;

        $result = self::request($url);
        $logs = Logs::add(1,$new, $old, $result);

        if($result == 'No old key') return [
            'error' => [
                'msg' => '<div class="alert alert-danger">
                            <strong>'.Lang::translate('ERROR').'!</strong>
                            '.Lang::translate('ERROR_NOT_FOUND_OLD_KEY').'
                        </div>'
            ]
        ];

        if($result == 'ok') return [
            'success' => [
                'title' => 'Успешно',
                'msg' => '<div class="alert alert-success">
                            <strong>'.Lang::translate('SUCCESS').'!</strong>
                            '.Lang::translate('SUCCESS_CHANGE_KEY').'
                        </div>'
            ]
        ];
    }

    public static function days($data){
        $key = !empty($data['key']) ? $data['key'] : false;
        $days = !empty($data['days']) ? $data['days'] : 0;

        $pass = \Yii::$app->params['func'][2]['password'];

        if(!$key) return [
            'error' => [
                'msg' => '<div class="alert alert-danger">
                            <strong>'.Lang::translate('ERROR').'!</strong>
                            '.Lang::translate('ERROR_EMPTY_KEY').'
                        </div>'
            ]
        ];

        if(!$days AND $days != 0) return [
            'error' => [
                'msg' => '<div class="alert alert-danger">
                            <strong>'.Lang::translate('ERROR').'!</strong>
                            '.Lang::translate('ERROR_EMPTY_DAYS').'
                        </div>'
            ]
        ];

        $url = 'http://islandlab.ru/soft/islandgrampr/addkFILM.php?days='.$days.'&admin=script&pass='.$pass.'&key='.$key;

        $log_text = '<b>'.Lang::translate('KEY').':</b> '.$key.'; <b>'.Lang::translate('DAYS').':</b> '.$days;

        $result = self::request($url);
        $logs = Logs::add(2,$key, false, $result, $log_text);

         if($result == 'ok') return [
            'success' => [
                'msg' => '<div class="alert alert-success">
                            <strong>'.Lang::translate('SUCCESS').'!</strong>
                            '.Lang::translate('SUCCESS_DAYS').'
                        </div>'
            ]
        ];
    }

    public static function request($url){
        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            $out = curl_exec($curl);

            curl_close($curl);
        }

        return $out;
    }
}
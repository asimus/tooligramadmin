<?php

namespace Tooligram\Controllers;

use Tooligram\Models\User;

class Auth {

    public static function login($data){

        $username = !empty($data['username']) ? $data['username'] : false;
        $password = !empty($data['password']) ? $data['password'] : false;

        if(!$password || !$username){
            return [
                'error' => [
                    'msg' => Lang::translate('EMPTY_LOGIN_DATA')
                ]
            ];
        }

        $User = User::findOne(['username' => $username, 'code' => 1]);
        if(!$User || !$User->validatePassword($password)) return [
            'error' => [
                'msg' => Lang::translate('WRONG_LOGIN_DATA')
            ]
        ];

        if(\Yii::$app->user->login($User, 3600*24*30)){
            return [
                'success' => [
                    'title' => Lang::translate('SUCCESS'),
                    'msg' => Lang::translate('SUCCESS_LOGIN')
                ]
            ];
        }
        else{
            return [
                'error' => [
                    'msg' => Lang::translate('ERROR_LOGIN')
                ]
            ];
        }
    }

    public static function signup($data){

        $username = !empty($data['username']) ? $data['username'] : false;
        $password = !empty($data['password']) ? $data['password'] : false;

        if(!$password || !$username){
            return [
                'error' => [
                    'msg' => Lang::translate('EMPTY_LOGIN_DATA')
                ]
            ];
        }

        $User = User::byUsername($username);
        if($User) return [
            'error' => [
                'msg' => Lang::translate('ISSET_THIS_USER')
            ]
        ];


        $User = new User;
        $User->username = $username;
        $User->setPassword($password);
        $User->ip_history = json_encode([
            $_SERVER['REMOTE_ADDR']
        ]);
        $User->code = 1;
        $User->generateAuthKey();
        if(!$User->save()) return [
            'error' => [
                'msg' => Lang::translate('UNABLE_TO_CREATE_ACCOUNT')
            ]
        ];

        return [
            'success' => [
                'title' => Lang::translate('SUCCESS_REG_SUPPORT_USER'),
                'msg' => '<b>Логин:</b> '.$username.'<br> <b>Пароль:</b> '.$password
            ]
        ];
    }

    public static function resetPassword($data){
        $old = !empty($data['old']) ? $data['old'] : false;
        $new = !empty($data['new']) ? $data['new'] : false;

        if(!$old || !$new){
            return [
                'error' => [
                    'msg' => Lang::translate('EMPTY_FILEDS')
                ]
            ];
        }

        $User = User::current();
        $User = User::findOne(['id' => $User->id, 'password' => hash('sha512',$old)]);

        if(!$User){
            return [
                'error' => [
                    'msg' => Lang::translate('ERROR_OLD_PASSWORD')
                ]
            ];
        }

        $User = User::current();
        $User->setPassword($new);
        if(!$User->save()) return [
            'error' => [
                'msg' => Lang::translate('ERROR_RESET_PASSWORD')
            ]
        ];

        return [
            'success' => [
                'title' => Lang::translate('SUCCESS'),
                'msg' =>  Lang::translate('SUCCESS_RESET_PASSWORD')
            ]
        ];
    }


    public static function logout(){
        if(\Yii::$app->user->logout()) return [
            'success' => true,
            'link' => '/'
        ];
        return [
            'error' => [
                'msg' => Lang::translate('LOGOUT_FAILED')
            ]
        ];
    }

}
<?php

namespace Tooligram\Models;

use yii\web\IdentityInterface;
use Tooligram\Controllers\Lang;

class User extends Tooligram implements IdentityInterface{

    /**
     * Search
     */

    public static function byUsername($username){
        return static::findOne([
            'username' => $username
        ]);
    }

    public static function byKey($key){
        return static::findOne([
            'authKey' => $key
        ]);
    }

    public static function deleteUser($data){
        $id = !empty($data['id']) ? $data['id'] : false;

        if(!$id){
            return [
                'error' => [
                    'msg' => Lang::translate('ERROR_DELETE_USER')
                ]
            ];
        }

        $user = self::findOne([
            'id' => $id
        ]);

        $user->code = 0;

        if($user->save()){
            return [
                'success' => [
                    'msg' => str_replace('%username%', $user->username, Lang::translate('SUCCESS_USER_DELETE'))
                ]
            ];
        }

        return [
            'error' => [
                'msg' => Lang::translate('ERROR_DELETE_USER')
            ]
        ];
    }

    public static function unlockUser($data){
        $id = !empty($data['id']) ? $data['id'] : false;

        if(!$id){
            return [
                'error' => [
                    'msg' => Lang::translate('ERROR_UNLOCK_USER')
                ]
            ];
        }

        $user = self::findOne([
            'id' => $id
        ]);

        $user->code = 1;

        if($user->save()){
            return [
                'success' => [
                    'msg' => str_replace('%username%', $user->username, Lang::translate('SUCCESS_USER_UNLOCK'))
                ]
            ];
        }

        return [
            'error' => [
                'msg' => Lang::translate('ERROR_UNLOCK_USER')
            ]
        ];
    }


    /**
     * Static
     */

    public static function current(){
        return \Yii::$app->user->identity;
    }

    public static function ChangeActiveAccount($data){

        $instagramID = !empty($data['id']) ? $data['id'] : false;
        if(!$instagramID) return [
            'error' => [
                'msg' => Lang::translate('EMPTY_FIELDS')
            ]
        ];

        $User = self::current();
        $Instagram = Instagram::byId($instagramID);

        if($Instagram->userID != $User->id) return [
            'error' => [
                'msg' => Lang::translate('RESTRICTED')
            ]
        ];

        $User->active_account = $Instagram->id;
        $User->save();

        return [
            'success' => true,
            'link' => "/"
        ];

    }

    public static function changePassword($data){

        $password = !empty($data['password']) ? $data['password'] : false;
        $repeat = !empty($data['repeat']) ? $data['repeat'] :false;
        if(!$password || !$repeat) return [
            'error' => [
                'msg' => Lang::translate('EMPTY_FIELDS')
            ]
        ];
        if($password != $repeat) return [
            'error' => [
                'msg' => Lang::translate('PASSWORD_MISMATCH')
            ]
        ];

        $User = self::current();
        $User->setPassword($password);
        if(!$User->save()) return [
            'error' => [
                'msg' => Lang::translate('SOMETHING_WRONG')
            ]
        ];

        return ['success' => true];
    }

    /**
     * Helpers
     */

    public function setPassword($password){
        $this->password = hash('sha512',$password);
    }

    public function generateAuthKey(){
        $key = \Yii::$app->security->generateRandomString(512);
        if(self::byKey($key)) return $this->generateAuthKey();
        $this->authKey = $key;
    }

    public function validatePassword($password){
        return hash('sha512',$password) == $this->password;
    }

    /**
     * Auth methods
     */

    public static function findIdentity($id){
        return static::findOne(
            ['id' => $id ]
        );
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        return static::findOne(
            ['authKey' => $token]
        );
    }

    public function getId() {
        return $this->id;
    }

    public function getAuthKey() {
        return $this->authKey;
    }

    public function validateAuthKey($authKey) {
        return $this->authKey === $authKey;
    }

    /**
     * Table Name
     */
    public static function tableName(){
        return 'users';
    }

}
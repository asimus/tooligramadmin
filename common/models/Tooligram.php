<?php

namespace Tooligram\Models;

use yii\db\ActiveRecord;

class Tooligram extends ActiveRecord {


    /**
     * Search
     */
    public static function byId($id){
        return self::findOne([
            'id' => $id
        ]);
    }


    public static function getDb(){
        return \Yii::$app->tooligram;
    }

}
<?php

namespace Tooligram\Models;

class Logs extends Tooligram {

    public static function add($func_id, $key, $oldkey, $result, $log_text = false){
        $User = User::current();

        $Logs = new Logs;
        $Logs->userID = $User->id;
        $Logs->funcID = $func_id;
        $Logs->timestamp = time();
        if($log_text){
            $Logs->key = $log_text;
        }else{
            $Logs->key = $key;
        }
        $Logs->oldkey = $oldkey;
        $Logs->result = $result;

        $Logs->save();
    }


    /**
     * Table Name
     */
    public static function tableName(){
        return 'logs';
    }

}
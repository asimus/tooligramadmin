<?php
return [

    'UNDEFINED' => 'UNDEFINED',

    'SUCCESS' => 'Успешно',
    'ERROR' => 'Ошибка',
    'KEY' => 'Ключ',
    'DAYS' => 'Дни',
    
    'QT_DAYS_KEY' => 'Количество дней ключа: ',

    'WRONG_LOGIN_DATA' => 'Неправильный логин или пароль',

    'SUCCESS_LOGIN' => 'Вы успешно авторизовались на сайте',
    'ERROR_LOGIN' => 'При попытке авторизоваться произошла неизвестная ошибка',

    'ERROR_EMPTY_DAYS' => 'Введите количество дней',
    'SUCCESS_DAYS' => 'Дни успешно выданы',

    'ERROR_EMPTY_KEY' => 'Введите ключ!',

    'ERROR_EMPTY_KEY_OLD_NEW' => 'Введите старый и новый ключ',
    'ERROR_NOT_FOUND_OLD_KEY' => 'Старый ключ не найден',
    'ERROR_NOT_FOUND_KEY' => 'Ключ не найден',
    'SUCCESS_CHANGE_KEY' => 'Ключ успешно изменен',

    'EMPTY_LOGIN_DATA' => 'Введите данные для регистрации: логин и пароль.',
    'ISSET_THIS_USER' =>  'Пользователь с таким логином уже существует в базе данных.',
    'UNABLE_TO_CREATE_ACCOUNT' => 'Невозможно создать аккаунт.',

    'SUCCESS_REG_SUPPORT_USER' => 'Успешная регистрация сотрудника.',

    'SUCCESS_USER_DELETE' => 'Сотрудник %username% удален',

    'ERROR_DELETE_USER' => 'Удаление не возможно. Ошибка доступа или не правильный ID сотрудника',

    'EMPTY_FILEDS' => 'Пожалуйста, заполните все поля',
    'ERROR_OLD_PASSWORD' => 'Пароль не изменён, так как старый пароль введён неправильно',
    'ERROR_RESET_PASSWORD' => 'Пароль не изменён. Что-то пошло не так :(',
    'SUCCESS_RESET_PASSWORD' => 'Пароль успешно изменен',

];
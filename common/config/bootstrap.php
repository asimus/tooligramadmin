<?php
/**
 * @project: Tooligram Admin
 * @author Marsel Salimhanov
 * @date: ********
 */

Yii::setAlias('tooligram', dirname(dirname(__DIR__)) . '/common');
Yii::setAlias('application', dirname(dirname(__DIR__)) . '/application');
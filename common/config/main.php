<?php
/**
 * @project: Megaposting
 * @author Marsel Salimhanov
 * @date: ********
 */

return [
    'basePath' => dirname(dirname(__DIR__)),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => ['log'],
    "components" => [
        'errorHandler' => [
            'errorAction' => 'panel/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'timeZone' => 'Europe/Moscow',
    'charset' => 'UTF-8',
    'language' => 'ru-RU',
    'params' => [
        'func' => [
            1 => [
                'name' => 'Перепривязка',
                'password' => 'jksdbf434jhb'
            ],
            2 => [
                'name' => 'Указать ключу дни',
                'password' => '10jksdbf434jhb01'
            ],

            3 => [
                'name' => 'Проверка дней'
            ],
        ],
    ],
];
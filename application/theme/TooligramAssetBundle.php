<?php


namespace Tooligram\Application\Theme;

use yii\web\AssetBundle;

class TooligramAssetBundle extends AssetBundle {

    public $publishOptions = [
        'linkAssets' => true
    ];

    public $sourcePath = '@application/theme/static/';

    public $css = [
        'css/sweetalert.css',
        'css/style.css',
        'css/media.css',
        'https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic&subset=latin,cyrillic-ext'
    ];
    public $js = [
        'js/external/hash/hash.js',
        'js/script.js',
        'js/sweetalert.min.js',

        'js/plugins/panel.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\bootstrap\BootstrapAsset',
        
    ];

}
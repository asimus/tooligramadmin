var tooligram = {
    request: function(data){
        return $.ajax({
            type: 'POST',
            url: '/ajax/'+data.action,
            data: {
                data: data.data
            }
        });
    },

    click: function($this){
        var plugin = $this.data('plugin');
        var action = $this.data('action');
        var form = $this.data('form');

        if(form) {
            var data = $("form[name=" + form + "]").serializeArray();
        }else{
            var data = false;
        }

        tooligram[plugin][action]($this, data);
    },

    loader: function(status,msg){
        var loader = $('.loader');
        if(status){
            loader.addClass('offload');
            loader.removeClass('onload');
        }else{
            loader.removeClass('offload');
            loader.addClass('onload');
        }

        if(msg){
            alert(msg);
        }
    },

};

$('.__click').on('click', function () {
    tooligram.click($(this));
});
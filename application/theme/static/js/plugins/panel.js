tooligram.panel = {
    check: function ($this, data) {
        var $btn = $($this).button('loading');
        var resultAlert = $('.result_check');

        var $key = $('#keyCheck').val();

        tooligram.request({
            action: 'check',
            data: {
                key: $key
            }
        }).success(function (result) {
            $btn.button('reset');

            if(result.error){
                resultAlert.html(result.error.msg);
            }

            if(result.success){
                resultAlert.html(result.success.msg);
            }
        });

        setTimeout(function(){
            resultAlert.html('');
        }, 3000);
    },

    support_reg: function ($this, data) {
        var $btn = $($this).button('loading');
        
        var username = $('#supportUsername').val();
        var password = $('#supportPassword').val();

        tooligram.request({
            action: 'signup',
            data: {
                username: username,
                password: password
            }

        }).success(function (result) {
            $btn.button('reset');

            if(result.error){
                swal("Ошибка!", result.error.msg, "error");
            }

            if(result.success){
                swal({
                    title: result.success.title,   
                    text: result.success.msg,   
                    html: true,
                    type: 'success'
                });
            }
        });
    },

    generate: function ($this, data) {
        var $btn = $($this).button('loading');

        var password = $('#supportPassword');

        tooligram.request({
            action: 'generate'
        }).success(function (result) {
            $btn.button('reset');

            password.val(result.success.password);
        });
    },

    deleteSupport: function ($this, data) {
        var $btn = $($this).button('loading');

        var id = $($this).data('id');
        swal({   
            title: "Вы уверены?",   
            text: "После подтверждения сотрудник будет удален из базы данных!",   
            type: "warning",   
            showCancelButton: true,
            showLoaderOnConfirm: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Подтвердить!", 
            cancelButtonText: "Отмена",  
            closeOnConfirm: false 
        }, function(){   
            tooligram.request({
                action: 'delete',
                data: {
                    id: id
                }
            }).success(function (result) {
                if(result.success){
                    swal("Успешно!", result.success.msg, "success"); 
                    $('#tr_'+id).remove();
                }

                if(result.error){
                    swal("Ошибка!", result.error.msg, "error"); 
                }
            });
        });

        $btn.button('reset');
    },

    unlockSupport: function ($this, data) {
        var $btn = $($this).button('loading');

        var id = $($this).data('id');
        
        tooligram.request({
            action: 'unlock',
            data: {
                id: id
            }
        }).success(function (result) {
            if(result.success){
                swal("Успешно!", result.success.msg, "success"); 
                $('#user_'+id).text('Заблокировать');
                $('#user_'+id).removeClass('success');
                $('#user_'+id).addClass('warning');
            }

            if(result.error){
                swal("Ошибка!", result.error.msg, "error"); 
            }
        });

        $btn.button('reset');
    },

    resetPassword: function ($this, data) {
        var $btn = $($this).button('loading');
        
        var oldpassword = $('#oldpassword').val();
        var newpassword = $('#newpassword').val();

        tooligram.request({
            action: 'reset',
            data: {
                old: oldpassword,
                new: newpassword
            }

        }).success(function (result) {
            $btn.button('reset');

            if(result.error){
                swal("Ошибка!", result.error.msg, "error");
            }

            if(result.success){
                swal({
                    title: result.success.title,   
                    text: result.success.msg,   
                    html: true,
                    type: 'success'
                });
            }
        });
    },

    login: function ($this, data) {
        var $btn = $($this).button('loading');

        var username = $('#username').val();
        var password = $('#password').val();

        tooligram.request({
            action: 'login',
            data: {
                username: username,
                password: password
            }
        }).success(function (result) {
            $btn.button('reset');

            if(result.error){
                swal("Ошибка!", result.error.msg, "error");
            }

            if(result.success){
                swal({
                    title: result.success.title,
                    text: result.success.msg,
                    type: "success"
                },function(){
                    location.href = "/admin";
                });
            }
        });
    },

    changeKey: function ($this, data) {
        var resultAlert = $('.result_change');
        var $btn = $($this).button('loading');

        var oldkey = $('#oldkey').val();
        var newkey = $('#newkey').val();

        tooligram.request({
            action: 'changekey',
            data: {
                old: oldkey,
                new: newkey
            }
        }).success(function (result) {
            $btn.button('reset');
            
            if(result.error){
                resultAlert.html(result.error.msg);
            }

            if(result.success){
                resultAlert.html(result.success.msg);
            }

            setTimeout(function(){
                resultAlert.html('');
            }, 3000);
        });
    },

    days: function ($this, data) {
        var resultAlert = $('.result_days');
        var $btn = $($this).button('loading');

        var key = $('#key').val();
        var days = $('#days').val();

        tooligram.request({
            action: 'days',
            data: {
                key: key,
                days: days
            }
        }).success(function (result) {
            $btn.button('reset');

            if(result.error){
                resultAlert.html(result.error.msg);
            }

            if(result.success){
                resultAlert.html(result.success.msg);
            }

            setTimeout(function(){
                resultAlert.html('');
            }, 3000);
        });
    },

    cleanChange: function($this, data){
        var oldkey = $('#oldkey').val('');
        var newkey = $('#newkey').val('');
    },

    cleanDays: function($this, data){
        var key = $('#key').val('');
        var days = $('#days').val('');
    },

    cleanCheck: function($this, data){
        var $key = $('#keyCheck').val('');
    },
};
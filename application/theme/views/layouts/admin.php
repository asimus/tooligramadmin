<?php


use Tooligram\Application\Theme\TooligramAssetBundle as Bundle;
use yii\helpers\Html;
use yii\widgets\Menu;

Bundle::register($this);

$imgPath = Yii::$app->assetManager->getPublishedUrl('@application/theme/static/')."/images";
?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html>
<head>

<!-- Title Start -->
<title><?= Html::encode($this->title) ?></title>
<!-- Title End -->

<!-- Meta Start -->
<meta http-equiv="content-type" content="text/html; charset=<?= Yii::$app->charset ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<?= Html::csrfMetaTags()?>

<!-- Meta End -->

<!-- Head Start -->
<?php $this->head();?>

<!-- Head End -->

</head>
<body>
<?php $this->beginBody();?>
<div class="container">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="/">Tooligram Admin</a>
            </div>
            <?php 
                echo Menu::widget([
                    'items' => [
                        ['label' => 'Управление', 'url' => ['admin/index']],
                        ['label' => 'Логи', 'url' => ['admin/logs']],
                        ['label' => 'Регистрация', 'url' => ['admin/reg']],
                        ['label' => 'Настройки', 'url' => ['admin/settings']]                                       
                    ],

                    'options' => [
                        'class' => 'nav navbar-nav'
                    ],

                    'activeCssClass'=>'active'
                    ]);
            ?>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="/account/logout">Выйти</a></li>
            </ul>

        </div><!-- /.container-fluid -->
    </nav> 
    <div class="row">
        <div class="col-md-12">
            <div class="content">
                <?php echo $content;?>
            </div>
        </div>   
    </div>
</div>
<?php $this->endBody();?>
</body>
</html>
<?php $this->endPage();?>
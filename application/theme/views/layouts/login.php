<?php


use Tooligram\Application\Theme\TooligramAssetBundle as Bundle;
use yii\helpers\Html;
use yii\widgets\Menu;

Bundle::register($this);

$imgPath = Yii::$app->assetManager->getPublishedUrl('@application/theme/static/')."/images";
?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html>
<head>

<!-- Title Start -->
<title><?= Html::encode($this->title) ?></title>
<!-- Title End -->

<!-- Meta Start -->
<meta http-equiv="content-type" content="text/html; charset=<?= Yii::$app->charset ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<?= Html::csrfMetaTags()?>

<!-- Meta End -->

<!-- Head Start -->
<?php $this->head();?>

<!-- Head End -->

</head>
<body>
<?php $this->beginBody();?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
                <?php echo $content;?>            
        </div>   
    </div>
</div>    

<?php $this->endBody();?>
    </body>
    </html>
<?php $this->endPage();?>
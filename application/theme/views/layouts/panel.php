<?php
/**
 * @project: Megaposting
 * @author Marsel Salimhanov
 * @date: ********
 */

use Megaposting\Application\Theme\MegaAssetBundle as Bundle;
use yii\helpers\Html;

Bundle::register($this);

$imgPath = Yii::$app->assetManager->getPublishedUrl('@application/theme/static/')."/images";
?>
<?php $this->beginPage()?>
    <!DOCTYPE html>
    <html>
    <head>

        <!-- Title Start -->
        <title><?= Html::encode($this->title) ?></title>
        <!-- Title End -->

        <!-- Meta Start -->
        <meta http-equiv="content-type" content="text/html; charset=<?= Yii::$app->charset ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags()?>

        <!-- Meta End -->

        <!-- Head Start -->
        <?php $this->head();?>

        <!-- Head End -->

    </head>
    <body>
    <?php $this->beginBody();?>

    <div class="container">
        <header class="topheader">
            <nav class="topmenu">
                <li class="topmenu_item">
                    <a href="/instagram/feed" class="topmenu_item_a">
                        <span class="glyphicon glyphicon-home topmenu_icon"></span>
                        Лента
                    </a>
                </li>
                <li class="topmenu_item">
                    <a href="/instagram/my" class="topmenu_item_a">
                        <span class="glyphicon glyphicon-user topmenu_icon"></span>
                        Моя страница
                    </a>
                </li>
                <li class="topmenu_item">
                    <a href="/instagram/posting" class="topmenu_item_a">
                        <span class="glyphicon glyphicon-camera topmenu_icon"></span>
                        Постинг
                    </a>
                </li>
                <li class="topmenu_item">
                    <a href="/panel/settings" class="topmenu_item_a">
                        <span class="glyphicon glyphicon-cog topmenu_icon"></span>
                        Настройки
                    </a>
                </li>
            </nav>
        </header>

        <div class="content">
            <div class="row">

                <div class="col-md-12">
                    <div class="content">
                        <?=$content?>
                        <div class="loader offload">
                            <div id="load" class="cssload-container">
                                <div class="cssload-double-torus"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->endBody();?>
    <script>
        
    </script>
    </body>
    </html>
<?php $this->endPage();?>
<?php
/**
 * @project: Megaposting
 * @author Marsel Salimhanov
 * @date: ********
 */

use yii\helpers\Html;

?>
<div class="panel panel-default">
	<div class="panel-heading">Перепривязка</div>
	<div class="panel-body">
		<div class="result_change"></div>
		<div class="panel_item">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">
					<span class="glyphicon glyphicon-barcode"></span>
				</span>
				<input type="text" id="oldkey" class="form-control" placeholder="Старый ключ" aria-describedby="basic-addon1">
			</div>
		</div>
		<div class="panel_item">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">
					<span class="glyphicon glyphicon-qrcode"></span>
				</span>
				<input type="text" id="newkey" class="form-control" placeholder="Новый ключ" aria-describedby="basic-addon1">
			</div>
		</div>
		<div class="panel_item">
			<button type="button" data-loading-text="Подождите..." autocomplete="off" class="btn btn-success __click" data-plugin="panel" data-action="changeKey" style="margin-bottom: 5px;">Перепривязать</button>
			<button type="button" data-loading-text="Подождите..." autocomplete="off" class="btn btn-success __click" data-plugin="panel" data-action="cleanChange" style="margin-bottom: 5px;">Очистить поля</button>
		</div>

	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">Указать ключу дни</div>
	<div class="panel-body">
	    <div class="result_days"></div>
		<div class="panel_item">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">
					<span class="glyphicon glyphicon-qrcode"></span>
				</span>
				<input type="text" id="key" class="form-control" placeholder="Ключ" aria-describedby="basic-addon1">
			</div>
		</div>
		<div class="panel_item" style="width: 100px;">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">
					<span class="glyphicon glyphicon-calendar"></span>
				</span>
				<input type="text" id="days" class="form-control" placeholder="Дни" aria-describedby="basic-addon1">
			</div>
		</div>
		<div class="panel_item">
			<button  type="button" data-loading-text="Подождите..." autocomplete="off" class="btn btn-success __click" data-plugin="panel" data-action="days" style="margin-bottom: 5px;">Выдать</button>
			<button type="button" data-loading-text="Подождите..." autocomplete="off" class="btn btn-success __click" data-plugin="panel" data-action="cleanDays" style="margin-bottom: 5px;">Очистить поля</button>
		</div>

	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">Проверка дней</div>
	<div class="panel-body">
	    <div class="result_check"></div>
		<div class="panel_item">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">
					<span class="glyphicon glyphicon-qrcode"></span>
				</span>
				<input id="keyCheck" type="text" class="form-control" placeholder="Ключ" aria-describedby="basic-addon1">
			</div>
		</div>
		
		<div class="panel_item">
			<button type="button" id="check" data-loading-text="Подождите..." autocomplete="off" class="btn btn-success __click" data-plugin="panel" data-action="check" style="margin-bottom: 5px;">Проверить</button>
			<button type="button" data-loading-text="Подождите..." autocomplete="off" class="btn btn-success __click" data-plugin="panel" data-action="cleanCheck" style="margin-bottom: 5px;">Очистить поле</button>
		</div>

	</div>
</div>

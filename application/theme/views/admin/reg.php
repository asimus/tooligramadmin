<?php

use Tooligram\Models\User;

$users = User::find()->where(['code' => 1])->andWhere(['role' => 'support'])->all();
?>
<div class="panel panel-default"  style="max-width: 350px;">
	<!-- Default panel contents -->
	<div class="panel-heading">Регистрация сотрудника</div>

	<div class="reg-block">
		<div class="panel_item">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">
					<span class="glyphicon glyphicon-user"></span>
				</span>
				<input type="text" id="supportUsername" class="form-control" placeholder="Логин" aria-describedby="basic-addon1">
			</div>
		</div>
		<div class="panel_item">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">
					<span class="glyphicon glyphicon-lock"></span>
				</span>
				<input type="text" id="supportPassword" class="form-control" placeholder="Пароль" aria-describedby="basic-addon1">
			</div>
		</div>
			<button type="button" id="generateBtn" data-loading-text="Подождите..." autocomplete="off" class="btn btn-success __click" data-plugin="panel" data-action="generate" style="margin-bottom: 5px;">Сгенерировать пароль</button>

			<button type="button" id="check" data-loading-text="Подождите..." autocomplete="off" class="btn btn-success __click" data-plugin="panel" data-action="support_reg">Зарегистрировать</button>
	</div>
</div>

<?php if($users){?>
<div class="panel panel-default"  style="max-width: 350px;">
	<!-- Default panel contents -->
	<div class="panel-heading">Сотрудники технической поддержки</div>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th width="20px">ID</th>
				<th>Логин</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($users as $user){?>
			<tr id="tr_<?=$user->id?>">
				<th>#<?=$user->id?></th>
				<th><?=$user->username?>
                    <button 
                        type="button" 
                        data-id="<?=$user->id?>"
                        class="btn btn-warning btn-xs __click" 
                        style="float: right;"
	                    data-loading-text="Подождите..." 
	                    autocomplete="off"
	                    data-plugin="panel" data-action="deleteSupport">
                        Удалить
                    </button>
				</th>
			</tr>
		<?php }?>
		</tbody>
</div>
<?php }?>
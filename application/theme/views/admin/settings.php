<div class="panel panel-default"  style="max-width: 350px;">
	<!-- Default panel contents -->
	<div class="panel-heading">Сменить пароль</div>

	<div class="reg-block">
	    <div class="panel_item">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">
					<span class="glyphicon glyphicon-lock"></span>
				</span>
				<input type="password" id="oldpassword" class="form-control" placeholder="Старый пароль" aria-describedby="basic-addon1">
			</div>
		</div>

		<div class="panel_item">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">
					<span class="glyphicon glyphicon-lock"></span>
				</span>
				<input type="password" id="newpassword" class="form-control" placeholder="Новый пароль" aria-describedby="basic-addon1">
			</div>
		</div>
		<button 
		    type="button" id="resetPassword" 
		    data-loading-text="Подождите..." 
		    autocomplete="off" 
		    class="btn btn-success __click" 
		    data-plugin="panel" 
		    data-action="resetPassword">
		    Сменить
		</button>
	</div>
</div>
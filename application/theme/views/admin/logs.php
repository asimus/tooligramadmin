<?php
/**
 * @project: Megaposting
 * @author Marsel Salimhanov
 * @date: ********
 */


use yii\helpers\Html;
use Tooligram\Models\Logs;
use Tooligram\Models\User;
use yii\widgets\LinkPager;

$func = \Yii::$app->params['func'];
?>
<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">Логи</div>
	<?php
	    if($logs){
	?>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Администратор</th>
				<th>Ключ</th>
				<th>Функция</th>
				<th>Результат</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($logs as $log) {
		?>	
		    <tr>
				<td><?=User::findOne(['id' => $log->userID])->username;?></td>
				<td>
                <?php if($log->oldkey){
                	echo '<b>Новый ключ:</b> '.$log->key.'; <b>Старый ключ:</b> '.$log->oldkey;
                }else{
                	echo $log->key;
                }?>
				</td>
				<td><?=$func[$log->funcID]['name']?></td>
				<td><?=$log->result?></td>
			</tr>
		<?php } ?>
		</tbody>
	</table>

	<?php    	
	    }
	?>
</div>
<?php
    echo LinkPager::widget([
		'pagination' => $pages,
	]);
?>
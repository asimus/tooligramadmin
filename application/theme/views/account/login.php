<div class="panel panel-default form_box">
	<!-- Default panel contents -->
	<div class="panel-heading">Вход</div>

	<div class="reg-block">
		<div class="panel_item">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">
					<span class="glyphicon glyphicon-user"></span>
				</span>
				<input type="text" id="username" class="form-control" placeholder="Логин" aria-describedby="basic-addon1">
			</div>
		</div>
		<div class="panel_item">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">
					<span class="glyphicon glyphicon-lock"></span>
				</span>
				<input type="password" id="password" class="form-control" placeholder="Пароль" aria-describedby="basic-addon1">
			</div>
		</div>
			<button type="button" id="login" data-loading-text="Подождите..." autocomplete="off" class="btn btn-success __click" data-plugin="panel" data-action="login">Войти</button>
	</div>
</div>
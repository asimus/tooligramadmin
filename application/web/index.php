<?php

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(dirname(dirname(__DIR__)) . '/vendor/autoload.php');
require(dirname(dirname(__DIR__)) . '/vendor/yiisoft/yii2/Yii.php');
require(dirname(dirname(__DIR__)) . '/common/config/bootstrap.php');

$config = \yii\helpers\ArrayHelper::merge(
    require dirname(dirname(__DIR__)) . "/common/config/main.php",
    require dirname(dirname(__DIR__)) . "/common/config/db.php",
    require dirname(__DIR__) . "/config/app.php"
);

$app = new yii\web\Application($config);
$app->run();
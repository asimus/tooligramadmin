<?php
/**
 * @project: Tooligram Admin
 * @author Marsel Salimhanov
 * @date: ********
 */

return [
    'id' => 'Tooligram',
    'name' => 'Tooligram Admin',
    'controllerNamespace' => 'Tooligram\Application\Controllers',
    'defaultRoute' => 'admin',
    'layout' => 'admin',
    'components' => [
        'request' => [
            'cookieValidationKey' => 'zOQ89pJF04kTXGxm4OSSSt_-9DHT0Us_',
        ],
        'view' => [
            'theme' => [
                'basePath' => '@application/theme/views',
                'pathMap' => [
                    '@app/views' => '@application/theme/views'
                ]
            ]
        ],
        'user' => [
            'identityClass' => 'Tooligram\Models\User'
        ],
        'urlManager' => [
            'rules' => [
                '/' => 'admin/index',
            ]
        ]
    ],
    'controllerMap' => []
];
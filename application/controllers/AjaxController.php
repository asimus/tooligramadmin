<?php

namespace Tooligram\Application\Controllers;

use Tooligram\Controllers\Helper;
use yii\web\Controller;
use yii\web\Response;
use Tooligram\Controllers\Auth;
use Tooligram\Controllers\Tooligram;
use Tooligram\Models\User;

class AjaxController extends Controller {

    private $data;

    public function beforeAction($event){
        $this->enableCsrfValidation = false;

        if(!parent::beforeAction($event)) return false;

        \Yii::$app->response->format = Response::FORMAT_JSON;

        if(!\Yii::$app->request->isPost){
            $msg = $this->actionIndex();
            \Yii::$app->end(json_encode($msg));
        }

        $this->data = \Yii::$app->request->post('data');

        return true;
    }

    public function actionIndex(){
        return [
            'result' => [
                'msg' => 'The Silence Is Coming.'
            ]
        ];
    }

    public function actionChangekey(){
        return Tooligram::changeKey($this->data);
    }

    public function actionDays(){
        return Tooligram::days($this->data);
    }

    public function actionLogin(){
        return Auth::login($this->data);
    }

    public function actionCheck(){
       return Tooligram::check($this->data);
    }

    public function actionSignup(){
        return Auth::signup($this->data);
    }

    public function actionReset(){
        return Auth::resetPassword($this->data);
    }

    public function actionDelete(){
        return User::deleteUser($this->data);
    }

    public function actionGenerate() {
        $password = "";

        $chr = [
            [64, 90],
            [97, 122]
        ];

        for ($i = 0; $i < 9; $i++){
            $chr2 = $chr[mt_rand(0, 1)];
            $password .= chr(mt_rand($chr2[0], $chr2[1])); // 97 - это a, а 122 - это z
        }

        return [ 
            'success' => [ 
                'password' => $password
            ]
        ];
  }

}
<?php

namespace Tooligram\Application\Controllers;

use yii\web\Controller;
use yii\web\View;

class AccountController extends Controller{

    public $layout = "login";

    public function beforeAction($action){

        if(!parent::beforeAction($action)){
            return false;
        }

        if(!\Yii::$app->user->isGuest){
            parent::redirect('/admin/',302);
        }

        return true;
    }

    public function actionIndex(){
        return parent::redirect("/account/login/",302);
    }

    public function actionLogin(){
        return $this->render('login');
    }

    public function actionSignup(){
        return $this->render('signup');
    }

    public function actionLogout(){
        if(\Yii::$app->user->logout())
            parent::redirect('/account/login',302);
    }
}
<?php
/**
 * @project: Tooligram Admin
 * @author Marsel Salimhanov
 * @date: ********
 */

namespace Tooligram\Application\Controllers;

use yii\web\Controller;
use yii\web\View;
use Tooligram\Models\User;
use Tooligram\Models\Logs;
use yii\data\Pagination;

class AdminController extends Controller {
    public $layout = 'admin';

    public function beforeAction($action){

        if(!parent::beforeAction($action)){
            return false;
        }

        if(\Yii::$app->user->isGuest){
            parent::redirect('/account/login/',302);
        }
        
        $User = User::current();
        if($action->id == 'logs' OR $action->id == 'reg'){
            if($User){
                if($User->role != 'admin')
                return parent::redirect('/',302);
            }
        }

        $pluginsPath = \Yii::$app->assetManager->getPublishedUrl('@application/theme/static/')."/js/";
        


        return true;
    }

    public function actionIndex(){
        return $this->render('index');
    }

    public function actionLogs(){
        // выполняем запрос
        $query = Logs::find();
        // делаем копию выборки
        $countQuery = clone $query;
        // подключаем класс Pagination, выводим по 10 пунктов на страницу
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);
        // приводим параметры в ссылке к ЧПУ
        $pages->pageSizeParam = false;
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('logs',[
         'logs' => $models,
         'pages' => $pages,
        ]);
    }

    public function actionReg(){
        return $this->render('reg');
    }

    public function actionSettings(){
        return $this->render('settings');
    }

}


